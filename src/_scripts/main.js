// Main javascript entry point
// Should handle bootstrapping/starting application
'use strict';
import { TinyEmitter } from 'tiny-emitter';

import Dropdown from '../_modules/molecules/dropdown/dropdown';

import SiteHeader from '../_modules/organisms/site-header/site-header';
import SiteFooter from '../_modules/organisms/site-footer/site-footer';
import HomeBanner from '../_modules/organisms/home-banner/home-banner';
import TabListing from '../_modules/organisms/tabs-listing/tabs-listing';
// import EventListing from '../_modules/organisms/event-listing/event-listing';
// import FilterListing from '../_modules/organisms/filtered-listing/filtered-listing';

$(() => {
  window.emitter = new TinyEmitter();

  new SiteHeader($('.site-header'));
  new SiteFooter($('.site-footer'));

  $('.home-banner').map((i, ele) => {
    new HomeBanner($(ele));
  });


  $('.eyd-custom-select').map((i, ele) => {
    new Dropdown($(ele));
  })

  $('.tabs-listing').map((i, ele) => {
    new TabListing($(ele));
  })

  // new EventListing();
  // new FilterListing();
});

