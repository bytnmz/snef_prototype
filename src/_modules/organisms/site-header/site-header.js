'use strict';

import Accordion from "../accordion/accordion";

export default class SiteHeader {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    const $navToggle = $('.site-header__menu-toggle', this.$selector);

    $navToggle.find('button').on('click', e => {
			e.preventDefault();

			if (!this.$selector.hasClass('active')) {
				this.$selector.addClass('active');
				$navToggle.addClass('collapse');

				setTimeout(() => {
					$navToggle.addClass('rotate');
				}, 200);
			}
			else {
				this.$selector.removeClass('active');
				$navToggle.removeClass('rotate');

				setTimeout(() => {
				  $navToggle.removeClass('collapse');
          $('li.active').find('[data-content]').hide();
          $('li.active').removeClass('active');
				}, 200);
			}
		});

    $('[data-toggle]', this.$selector).map((i, ele) => {
			new Accordion($(ele));
		});

    let timer;
    $(window).on('resize', () => {
      clearTimeout(timer);

      timer = setTimeout(() => {
        $('.site-header').removeClass('active');
        $('li.active', this.$selector).find('[data-content]').hide();
        $('li.active', this.$selector).removeClass('active');
        $navToggle.removeClass('collapse rotate');
      }, 300);
    });
  }
}
