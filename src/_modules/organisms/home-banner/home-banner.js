'use strict';

export default class HomeBanner {
  constructor($selector) {
    this.$selector = $selector;
    this.$image = $('.home-banner__image', $selector);
    this.threshold = this.$selector.outerHeight() - this.$image.height() - 98;

    $(window).on('load', () => {
      this.init();
    });

    $(window).on('resize', () => {
      this.threshold = this.$selector.outerHeight() - this.$image.height() - 98;
      this.init();
    });
  }

  init() {
    if($(window).width() >= 1024) {
      $(window).on('scroll.handleBanner', () => {
        this.handleScroll();
      });
    }
    else {
      $(window).off('scroll.handleBanner');
    }

    if($(window).scrollTop() >= 0) {
      this.$image.addClass('fix');
      
      if($(window).scrollTop() >= this.threshold) {
        this.$image.addClass('unfix');
      }
    }

    $('.home-banner__anchor', this.$selector).on('click', (e) => {
      e.preventDefault();

      $('html, body').stop().animate({
        scrollTop: $(`${$(e.currentTarget).attr('href')}`).offset().top - 32 - $('.site-header').outerHeight()
      }, 600);
    });
  }

  handleScroll() {
    if($(window).scrollTop() >= this.threshold) {
      this.$image.addClass('unfix');
    }
    else {
      this.$image.removeClass('unfix');
    }
  }
}
