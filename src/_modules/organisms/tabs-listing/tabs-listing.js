'use strict';

export default class TabsListing {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    const $tabNavigation = this.$selector.find('.tabs-listing__tabs'),
      $tabContents = this.$selector.find('.tabs-listing__content');

    // Tab functionality
		$('li a', $tabNavigation).map((i, ele) => {
			let target = $(ele).attr('href'),
				$target = $(`${target}`, $tabContents),
        text = $(ele).text();

			$(ele).on('click', (e) => {
				e.preventDefault();

				if(!$target.hasClass('active')){
					// Remove 'active' class from current active tab
					$('li.active', $tabNavigation).removeClass('active');
					// Add 'active' class to clicked tab
					$(ele).parent().addClass('active');

					// Fade out any active content
					$('div.active', $tabContents).fadeOut(300, () => {
						// Then remove 'active' class from previously active content
						$('div.active', $tabContents).removeClass('active');

						// Fade in target and attach 'active' class
						$target.fadeIn(300);
						$target.addClass('active');
					});

          $('> button > span', $tabNavigation).text(text);
				}
				else {
          e.stopPropagation();
					return;
				}
			});
		});

    $('> button', $tabNavigation).on('click', (e) => {
      e.preventDefault();
      e.stopPropagation();

      if(!$('nav', $tabNavigation).hasClass('active')) {
        $('nav', $tabNavigation).addClass('active');
      }
    });

    $(document).on('click', () => {
      $('nav', $tabNavigation).removeClass('active');
    })

    let timer;
    $(window).on('resize', () => {
      clearTimeout(timer);

      timer = setTimeout(() => {
        $('nav', $tabNavigation).removeClass('active');
      }, 300);
    });
  }
}
