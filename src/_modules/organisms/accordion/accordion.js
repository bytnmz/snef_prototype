'use strict';

export default class Accordion {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
		const $content = $('[data-content]', this.$selector.parent());

		this.$selector.on('click', e => {
			e.stopPropagation();

			if (!this.$selector.parent().hasClass('active')){
				this.$selector.parent().addClass('active');
				$content.slideDown(300);
			}
			else {
				this.$selector.parent().removeClass('active');
				$content.slideUp(300);
			}
		});
  }
}
