'use strict';

export default class SiteFooter {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if (selfInit) this.init();
  }

  init() {
    const $scrollTop = $('.site-footer__back-to-top', this.$selector);

    $scrollTop.on('click', function (e) {
      e.preventDefault();

      $('html, body').animate({
        scrollTop: 0
      }, 1000);
    });
  }
}
